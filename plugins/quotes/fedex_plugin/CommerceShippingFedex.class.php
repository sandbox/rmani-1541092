<?php

ini_set("soap.wsdl_cache_enabled", "0"); 

class CommerceShippingFedex extends CommerceShippingQuote {
  /**
   * Settings form to configure Fedex quoting service
   */
  public function settings_form(&$form, $rules_settings) {
    $form['store-info'] = array(
      '#title' => 'Store information',
      '#type' => 'fieldset'
    );

    $form['store-info']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['name']) ? $rules_settings['store-info']['name'] : '',
    );

    $form['store-info']['owner'] = array(
      '#type' => 'textfield',
      '#title' => t('Owner'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['owner']) ? $rules_settings['store-info']['owner'] : '',
    );

    $form['store-info']['email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email Address'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['email']) ? $rules_settings['store-info']['email'] : '',
    );

    $form['store-info']['phone'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['phone']) ? $rules_settings['store-info']['phone'] : '',
    );

    $form['store-info']['fax'] = array(
      '#type' => 'textfield',
      '#title' => t('Fax'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['fax']) ? $rules_settings['store-info']['fax'] : '',
    );

    $form['store-info']['street1'] = array(
      '#type' => 'textfield',
      '#title' => t('Street #1'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['street1']) ? $rules_settings['store-info']['street1'] : '',
    );

    $form['store-info']['street2'] = array(
      '#type' => 'textfield',
      '#title' => t('Street #2'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['street2']) ? $rules_settings['store-info']['street2'] : '',
    );

    $form['store-info']['city'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['city']) ? $rules_settings['store-info']['city'] : '',
    );

    $form['store-info']['zone'] = array(
      '#type' => 'textfield',
      '#title' => t('State/Province'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['zone']) ? $rules_settings['store-info']['zone'] : '',
    );

    $form['store-info']['postal_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Postal Code'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['postal_code']) ? $rules_settings['store-info']['postal_code'] : '',
    );

    $form['store-info']['country'] = array(
      '#type' => 'textfield',
      '#title' => t('Country'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['country']) ? $rules_settings['store-info']['country'] : 'US',
    );

    $form['shipment-settings'] = array(
      '#type' => 'fieldset',
      '#title' => 'Shipment Settings',
    );


    $form['shipment-settings']['fedex_services'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Fedex Services'),
      '#description' => t('Select the Fedex services that are available to customers.'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['fedex_services']) ? $rules_settings['shipment-settings']['fedex_services'] : array(),
      '#options' => _commerce_shipping_fedex_service_list(),
    );

    $form['shipment-settings']['fedex-markup-type'] = array(
      '#type' => 'select',
      '#title' => t('Markup type'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['fedex-markup-type']) ? $rules_settings['shipment-settings']['fedex-markup-type'] : 'percentage',
      '#options' => array(
        'percentage' => t('Percentage (%)'),
        'multiplier' => t('Multiplier (×)'),
        'currency' => t('Addition ($)'),
      ),
    );

    $form['shipment-settings']['fedex-markup'] = array(
      '#type' => 'textfield',
      '#title' => t('Shipping rate markup'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['fedex-markup']) ? $rules_settings['shipment-settings']['fedex-markup'] : '0',
      '#description' => t('Markup shipping rate quote by currency amount, percentage, or multiplier.'),
    );

    $form['shipment-settings']['currency_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Currency Code'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['currency_code']) ? $rules_settings['shipment-settings']['currency_code'] : 'USD',
    );

    $form['connection-settings'] = array(
      '#type' => 'fieldset',
      '#title' => 'Fedex Connection Settings',
    );

   $form['connection-settings']['fedex_connection_address'] = array(
      '#type' => 'textfield',
      '#title' => t('Connection Address'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['fedex_connection_address']) ? $rules_settings['connection-settings']['fedex_connection_address'] : 'https://wsbeta.fedex.com:443/web-services',
    ); 

    $form['connection-settings']['fedex_user_name'] = array(
      '#type' => 'textfield',
      '#title' => t('User name'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['fedex_user_name']) ? $rules_settings['connection-settings']['fedex_user_name'] : '',
      '#description' => t('The user name for your FedEx account.'),
    );
   $form['connection-settings']['fedex_password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['fedex_password']) ? $rules_settings['connection-settings']['fedex_password'] : '',
      '#description' => t('The password for your FedEx account.'),
    );
  $form['connection-settings']['fedex_Accountnumber'] = array(
      '#type' => 'textfield',
      '#title' => t('Accountnumber'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['fedex_Accountnumber']) ? $rules_settings['connection-settings']['fedex_Accountnumber'] : '',
      '#description' => t('The Accountnumber for your FedEx account.'),
    );
 $form['connection-settings']['fedex_meternumber'] = array(
      '#type' => 'textfield',
      '#title' => t('meternumber'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['fedex_meternumber']) ? $rules_settings['connection-settings']['fedex_meternumber'] : '',
      '#description' => t('The meternumber for your FedEx account.'),
    );
 $form['connection-settings']['fedex_integratorid'] = array(
      '#type' => 'textfield',
      '#title' => t('integratorid'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['fedex_integratorid']) ? $rules_settings['connection-settings']['fedex_integratorid'] : '',
      '#description' => t('The integratorid for your FedEx account.'),
    );
  }

  /**
   * Checkout Form for selecting FedEx shipment method
   */
  public function submit_form($pane_values, $checkout_pane, $order = NULL) {
    if (empty($order)) {
      $order = $this->order;
    }
    $form = parent::submit_form($pane_values, $checkout_pane, $order);
 
    // Merge in values from the order.
    if (!empty($order->data['commerce_shipping_fedex'])) {
      $pane_values += $order->data['commerce_shipping_fedex'];
    }

    $method = NULL;
    $all_methods = _commerce_shipping_fedex_service_list();
    $methods = array();
    foreach ($this->settings['shipment-settings']['fedex_services'] as $key => $service) {
      if (!$method) 
        $method = $key;
      if ($service !== 0) {
        $methods[$key] = $all_methods[$key];
      }
    }

    // Merge in default values.
    $pane_values += array(
      'method' => $method,
    );

    $form['method'] = array(
      '#type' => 'radios',
      '#title' => t('Shipment Method'),
      '#default_value' => $method,
      '#options' => $methods,
    );

 $form['res'] = array(
      '#type' => 'checkbox',
      '#title' => t('Residential'),
      '#default_value' => true

    );

    return $form;
  }

  /**
   * Validation form.
   */
  public function submit_form_validate($pane_form, $pane_values, $form_parents = array(), $order = NULL) {
  }

  /**
   * Calculate Quote
   */
  public function calculate_quote($currency_code, $form_values = array(), $order = NULL, $pane_form = NULL, $pane_values = NULL) {
    $method = $form_values['method'];
    $residential=$form_values['res'];
    $shipping_address = $pane_values['values']['customer_profile_shipping']['commerce_customer_address'][LANGUAGE_NONE][0];
    $rate = $this->fedex_quote($order, $method, $shipping_address,$residential);

    $all_methods = _commerce_shipping_fedex_service_list();

    if (empty($order)) {
      $order = $this->order;
    }
    $settings = $this->settings;
    $shipping_line_items = array();
    $shipping_line_items[] = array(
      'amount' => commerce_currency_decimal_to_amount($rate, $currency_code),
      'currency_code' => $currency_code,
      'label' => t($all_methods[$method]),
    );

    return $shipping_line_items;
  }

 
  private function fedex_quote($order, $method, $shipping_address,$residential) {
    $store_info = $this->settings['store-info'];
   /* $shipment_settings = $this->settings['shipment-settings'];
    $connection_settings = $this->settings['connection-settings'];
*/
    $shipment_weight = 0;
    foreach ($order->commerce_line_items[LANGUAGE_NONE] as $order_line) {
      $line_item_id = $order_line['line_item_id'];
      $line_item = commerce_line_item_load($line_item_id);
      $product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);
//drupal_set_message($product->field_weight[LANGUAGE_NONE][0]['weight'],'warning');
      $product_weight = isset($product->field_weight[LANGUAGE_NONE]) ? $product->field_weight[LANGUAGE_NONE][0]['weight'] : 0;
      $weight = $product_weight * $line_item->quantity;
      $shipment_weight += $weight;
    }

    $ounces = $shipment_weight - floor($shipment_weight);
    $ounces = 16 * $ounces;
    $pounds = floor($shipment_weight);

   $shipto_zip = $shipping_address['postal_code'];
    $shipfrom_zip = $store_info['postal_code'];
 if ($method == 'GROUND_HOME_DELIVERY')
{
$residential=true;
}

drupal_set_message($method);
//drupal_set_message($store_info['city']);
   /* $data = "<Package ID=\"1ST\"><Service>{$method}</Service><ZipOrigination>{$shipfrom_zip}</ZipOrigination><ZipDestination>{$shipto_zip}</ZipDestination><Pounds>{$pounds}</Pounds><Ounces>{$ounces}</Ounces><Size>REGULAR</Size><Machinable>TRUE</Machinable></Package>";
//    $request = $this->fedex_access_request($data);*/

require_once(drupal_get_path('module','commerce_shipping_fedex').'/plugins/quotes/fedex_plugin/library/fedex-common.php5');
$path_to_wsdl = drupal_get_path('module','commerce_shipping_fedex').'/plugins/quotes/fedex_plugin/wsdl/RateService_v10.wsdl';
try
{
if (!@file_get_contents($path_to_wsdl))
{
 throw new SoapFault('server','No WSDL found');
}
$client = new SoapClient($path_to_wsdl,array('trace'=>1,'exception'=>0)); 
$request['WebAuthenticationDetail'] = array(
	'UserCredential' =>array(
		'Key' => getProperty('key'), 
		'Password' => getProperty('password')
	)
); 
$request['ClientDetail'] = array(
	'AccountNumber' => getProperty('shipaccount'), 
	'MeterNumber' => getProperty('meter')
);
$request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request v10 using PHP ***');
$request['Version'] = array(
	'ServiceId' => 'crs', 
	'Major' => '10', 
	'Intermediate' => '0', 
	'Minor' => '0'
);
$request['ReturnTransitAndCommit'] = true;
$request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; 
$request['RequestedShipment']['ShipTimestamp'] = date('c');
$request['RequestedShipment']['ServiceType'] = $method;
$request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING';
$request['RequestedShipment']['TotalInsuredValue']=array('Ammount'=>0,'Currency'=>'USD');
$request['RequestedShipment']['Shipper'] =array(		'Contact' => array(
			'PersonName' => 'Sender Name',
			'CompanyName' => 'Sender Company Name',
			'PhoneNumber' => '9012638716'),		'Address' => array(
			'StreetLines' => array('Address Line 1'),
			'City' => $store_info['city'],
			'StateOrProvinceCode' =>  $store_info['zone'],
			'PostalCode' => $store_info['postal_code'],
			'CountryCode' => $store_info['country']));
$request['RequestedShipment']['Recipient'] = array(
		'Contact' => array(
			'PersonName' => 'Recipient Name',
			'CompanyName' => 'Company Name',
			'PhoneNumber' => '9012637906'
		),
		'Address' => array(
			'StreetLines' => array('Address Line 1'),
			'City' => $shipping_address['locality'],
			'StateOrProvinceCode' => $shipping_address['administrative_area'],
			'PostalCode' => $shipping_address['postal_code'],
			'CountryCode' => $shipping_address['country'],
			'Residential' => $residential)	);
$request['RequestedShipment']['ShippingChargesPayment'] = array(
		'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
		'Payor' => array(
			'AccountNumber' => getProperty('billaccount'),
			'CountryCode' => $store_info['country'])	);
$request['RequestedShipment']['RateRequestTypes'] = 'ACCOUNT'; 
$request['RequestedShipment']['RateRequestTypes'] = 'LIST'; 
$request['RequestedShipment']['PackageCount'] = '1';
$request['RequestedShipment']['RequestedPackageLineItems'] = array(
		'SequenceNumber'=>1,
		'GroupPackageCount'=>1,
		'Weight' => array(
			'Value' => $pounds,
			'Units' => 'LB'
		),
		
	);

	if(setEndpoint('changeEndpoint'))
	{
		$newLocation = $client->__setLocation(setEndpoint('endpoint'));
	}
$response = $client ->getRates($request);
drupal_set_message($response -> HighestSeverity);
if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR')
    {
$rateReply = $response -> RateReplyDetails;
  $amount =  number_format($rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount,2,".",",");
drupal_set_message($amount);
return $amount;
}
else
{
return NULL;
}

}catch (SoapFault $e)
{
drupal_set_message($e);
}

  }


function addShipper(){

	$shipper = array(
		'Contact' => array(
			'PersonName' => 'Sender Name',
			'CompanyName' => 'Sender Company Name',
			'PhoneNumber' => '9012638716'),
		'Address' => array(
			'StreetLines' => array('Address Line 1'),
			'City' => 'Austin',
			'StateOrProvinceCode' => 'TX',
			'PostalCode' => '73301',
			'CountryCode' => 'US')
	);
	return $shipper;
}
public function addRecipient(){
	$recipient = array(
		'Contact' => array(
			'PersonName' => 'Recipient Name',
			'CompanyName' => 'Company Name',
			'PhoneNumber' => '9012637906'
		),
		'Address' => array(
			'StreetLines' => array('Address Line 1'),
			'City' => 'Richmond',
			'StateOrProvinceCode' => 'BC',
			'PostalCode' => 'V7C4V4',
			'CountryCode' => 'CA',
			'Residential' => false)
	);
	return $recipient;	                                    
}
function addShippingChargesPayment(){
	$shippingChargesPayment = array(
		'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
		'Payor' => array(
			'AccountNumber' => getProperty('billaccount'),
			'CountryCode' => 'US')
	);
	return $shippingChargesPayment;
}
function addLabelSpecification(){
	$labelSpecification = array(
		'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
		'ImageType' => 'PDF',  // valid values DPL, EPL2, PDF, ZPLII and PNG
		'LabelStockType' => 'PAPER_7X4.75');
	return $labelSpecification;
}
function addSpecialServices(){
	$specialServices = array(
		'SpecialServiceTypes' => array('COD'),
		'CodDetail' => array(
			'CodCollectionAmount' => array('Currency' => 'USD', 'Amount' => 150),
			'CollectionType' => 'ANY')// ANY, GUARANTEED_FUNDS
	);
	return $specialServices; 
}
function addPackageLineItem1(){
	$packageLineItem = array(
		'SequenceNumber'=>1,
		'GroupPackageCount'=>1,
		'Weight' => array(
			'Value' => 50.0,
			'Units' => 'LB'
		),
		'Dimensions' => array(
			'Length' => 108,
			'Width' => 5,
			'Height' => 5,
			'Units' => 'IN'
		)
	);
	return $packageLineItem;
}

  /**
   * Return XML access request to be prepended to all requests to the fedex webservice.
  
  private function fedex_access_request($data) {
    $user_name = $this->settings['connection-settings']['fedex_user_name'];
    $password=$this->settings['connection-settings']['fedex_pasword'];
 $AccountNumber=$this->settings['connection-settings']['fedex_Accountnumber'];
    $Meternumber=$this->settings['connection-settings']['fedex_meternumber'];
 $integrate=$this->settings['connection-settings']['fedex_integratorid'];
    return "API=RateV3&XML=<RateV3Request USERID=\"$user_name\">". $data ."</RateV3Request>";
  } */

  /**
   * Modify the rate received from fedex before displaying to the customer.
   */
  private function fedex_markup($rate) {
//drupal_set_message($rate);
    $markup = $this->settings['shipment-settings']['fedex-markup'];
    $type = $this->settings['shipment-settings']['fedex-markup-type'];
    if (is_numeric(trim($markup))) {
      switch ($type) {
        case 'percentage':
          return $rate + $rate * floatval(trim($markup)) / 100;
        case 'multiplier':
          return $rate * floatval(trim($markup));
        case 'currency':
          return $rate + floatval(trim($markup));
      }
    }
    else {
      return $rate;
    }
  }

}



/**
 * Convenience function to get fedex codes for their services.
 */
function _commerce_shipping_fedex_service_list() {
  return array(
    'FEDEX_2_DAY' => 'FedEx 2 Day',
    'FEDEX_GROUND' => 'FedEx Ground',
    'FEDEX_EXPRESS_SAVER' => 'FedEx Express Saver',
    'GROUND_HOME_DELIVERY'=>'Ground Home Delivery',
'STANDARD_OVERNIGHT' => 'Standard Overnight'   
  );
}

/*function _commerce_shipping_fedex_Packaging_list() {
  return array(
    'FEDEX_BOX' => 'FedEx Box',
    'FEDEX_ENVELOPE' => 'FedEx Envelope',
    'FEDEX_PAK' => 'FedEx Package',
    'YOUR_PACKAGING'=>'OUR PACKAGING'
  );
} */

/**
 * Pseudo-constructor to set default values of a package.
 */
function _commerce_shipping_fedex_new_package() {
  $package = new stdClass();

  $package->weight = 0;
  $package->price = 0;

  $package->length = 0;
  $package->width = 0;
  $package->height = 0;

  $package->length_units = 'in';
  $package->weight_units = 'lb';
  $package->qty = 1;
  $package->pkg_type = '02';

  return $package;
}

